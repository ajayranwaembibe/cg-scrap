#!/usr/bin/env python
# coding: utf-8

# In[ ]:

from pymongo import MongoClient
import pandas as pd
import csv
import os
import time
import openpyxl
import re
from bson.regex import Regex
from bson.son import SON
from update_sheet import *
import time

# In[2]:

mongo_uri = "mongodb://prdcontentgrailembiberuser:AfrogpWiopif=123@10.18.6.18:27017,10.18.6.19:27017,10.18.6.21:27017/admin?authSource=admin&replicaSet=rs0&readPreference=primary&authMechanism=DEFAULT&ssl=false"

def learning_objects_collection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]

    return database["learning_objects"]

def learning_maps_collection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]

    return database["learning_maps"]

def learning_map_formats_collection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]

    return database["learning_map_formats"]
    
#def learning_objects_collection():
#    client = MongoClient("mongodb://prdcontentgrailembiberuser:AfrogpWiopif=123@10.18.6.18:27017,10.18.6.19:27017,10.18.6.21:27017/admin?authSource=admin&replicaSet=rs0&readPreference=primary&authMechanism=DEFAULT&ssl=false")
#    database = client["contentgrail"]
#
#    return database["learning_objects"]

#def learning_maps_collection():
#    client = MongoClient("mongodb://prdcontentgrailembiberuser:AfrogpWiopif=123@10.18.6.18:27017,10.18.6.19:27017,10.18.6.21:27017/admin?authSource=admin&replicaSet=rs0&readPreference=primary&authMechanism=DEFAULT&ssl=false")
#    database = client["contentgrail"]

#    return database["learning_maps"]

#def learning_map_formats_collection():
#    client = MongoClient("mongodb://prdcontentgrailembiberuser:AfrogpWiopif=123@10.18.6.18:27017,10.18.6.19:27017,10.18.6.21:27017/admin?authSource=admin&replicaSet=rs0&readPreference=primary&authMechanism=DEFAULT&ssl=false")
#    database = client["contentgrail"]

#    return database["learning_map_formats"]

def kv_entitycollection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]
    


def main():
    query = {}
    query["locales.en.status"] = "Published"
    query["type"] = "Video"

    projection = {"id":1,"locales.en.content.augmented_attributes.app_title":1,
                 "locales.en.content.augmented_attributes.app_description":1,
                  "locales.en.content.key_attributes.video_type":1,
                 "content.key_attributes.type":1,"locales.en.content.key_attributes.source":1,
                 "locales.en.content.key_attributes.url":1,"locales.en.content.source_attributes.duration":1,
                 "content.question_meta_tags.primary_concept":1,"content.question_meta_tags.secondary_concept":1,
                 "locales.en.content.custom_attributes.thumbnail":1,
                 "locales.en.content.custom_attributes.bg_thumbnail":1, "content.question_meta_tags":1}

    collection = learning_objects_collection()
    cursor = collection.find(query)#,projection=projection)#.limit(1000)

    i = 0
    videos = []
    for doc in cursor:
        i += 1
        
        video_meta = {}
        question_meta_tags = doc.get("content",{}).get("question_meta_tags",[])
        
        primary_concept = None
        secondary_concepts = []
        if len(question_meta_tags) > 0:
            primary_concept = question_meta_tags[0].get("primary_concept")
            secondary_concepts.extend(question_meta_tags[0].get("secondary_concept",[]))
        
        no_of_concepts = len(secondary_concepts)
        if primary_concept != None:
            no_of_concepts += 1
        
        video_meta["video_id"] = doc.get("id")
        augmented_attributes = doc.get("locales",{}).get("en",{}).get("content",{}).get("augmented_attributes",{})
        key_attributes = doc.get("locales",{}).get("en",{}).get("content",{}).get("key_attributes",{})
        source_attributes = doc.get("locales",{}).get("en",{}).get("content",{}).get("source_attributes",{})
        custom_attributes = doc.get("locales",{}).get("en",{}).get("content",{}).get("custom_attributes",{})
        
        video_meta["title"] = augmented_attributes.get("app_title")
        video_meta["description"] = augmented_attributes.get("app_description")
        video_meta["video_type"] = key_attributes.get("video_type")
        video_meta["type"] = doc.get("content",{}).get("key_attributes",{}).get("type")
        video_meta["source"] = key_attributes.get("source")
        video_meta["url"] = key_attributes.get("url")
        video_meta["duration"] = source_attributes.get("duration")
        video_meta["status"] = "Published"
        video_meta["primary_concept"] = primary_concept
        video_meta["secondary_concepts"] = secondary_concepts
        video_meta["No. of Concepts"] = no_of_concepts
        video_meta["thumbnail"] = custom_attributes.get("thumbnail")
        video_meta["bg_thumbnail"] = custom_attributes.get("bg_thumbnail")

        lms = []
        question_meta_tags = doc.get("content", {}).get("question_meta_tags", [])
        for qmt in question_meta_tags:
            lms.extend(qmt.get("learning_maps", []))

        video_meta["No of lms"] = len(lms)
        video_meta["LM's"]=lms

        videos.append(video_meta)

    print(i)
    df = pd.DataFrame(videos)
    df.to_csv("Video - Concept.csv",index=False)

    return df

i = 0
while True:
    i += 1
    df_new = main()
    update_sheet_by_df("1sjts7ib55h2qxXaYRjjWAa97QY1rgp8Li-OFdy_BX-k",df_new,"Video - Concept")
    print("round ",i," completed")
    time.sleep(82800)

