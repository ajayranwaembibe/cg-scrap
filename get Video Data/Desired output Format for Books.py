#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pymongo import MongoClient
import pandas as pd
import csv
import os
import time
import openpyxl
import re
from bson.regex import Regex
from bson.son import SON
from update_sheet import *
import time

# In[2]:


def learning_objects_collection():
    client = MongoClient("mongodb://vishalqaruser:RedftqaztTeffrwdredtf4rews5@vm-preprod-cgmongodb-001.srv.embibe.com:27017,vm-preprod-cgmongodb-002.srv.embibe.com:27017,vm-preprod-cgmongodb-003.srv.embibe.com:27017/contentgrail?authSource=admin&replicaSet=rs0&readPreference=secondaryPreferred")
    database = client["contentgrail"]

    return database["learning_objects"]

def learning_maps_collection():
    client = MongoClient("mongodb://vishalqaruser:RedftqaztTeffrwdredtf4rews5@vm-preprod-cgmongodb-001.srv.embibe.com:27017,vm-preprod-cgmongodb-002.srv.embibe.com:27017,vm-preprod-cgmongodb-003.srv.embibe.com:27017/contentgrail?authSource=admin&replicaSet=rs0&readPreference=secondaryPreferred")
    database = client["contentgrail"]

    return database["learning_maps"]

def learning_map_formats_collection():
    client = MongoClient("mongodb://vishalqaruser:RedftqaztTeffrwdredtf4rews5@vm-preprod-cgmongodb-001.srv.embibe.com:27017,vm-preprod-cgmongodb-002.srv.embibe.com:27017,vm-preprod-cgmongodb-003.srv.embibe.com:27017/contentgrail?authSource=admin&replicaSet=rs0&readPreference=secondaryPreferred")
    database = client["contentgrail"]

    return database["learning_map_formats"]

def kv_entitycollection():
    client = MongoClient("mongodb://vishalqaruser:RedftqaztTeffrwdredtf4rews5@vm-preprod-cgmongodb-001.srv.embibe.com:27017,vm-preprod-cgmongodb-002.srv.embibe.com:27017,vm-preprod-cgmongodb-003.srv.embibe.com:27017/contentgrail?authSource=admin&replicaSet=rs0&readPreference=secondaryPreferred")
    database = client["contentgrail"]

    return database["kv_entity"]


# In[5]:


def getFormatData():
    book_codes_in_scope = ["TN06M050","TN06M051","TN06M052","TN06S051","TN06S052","TN06S053","TN07M001","TN07M002","TN07M003","TN07S002","TN07S003","TN07S004","TN08M001","TN08M053","TN08M054","TN08S053","TN08S054","TN08S055","TN09M055","TN09S052","TN10M051","TN10S001","TN11M051","TN11M052","TN11M053","TN11P051","TN11P052","TN11C051","TN11C052","TN11B001","TN11B052","TN11B053","TN11B054","TN12M051","TN12M052","TN12M054","TN12P051","TN12P052","TN12C051","TN12C052","TN12B051","TN12B053","KL06M001","KL06M002","KL06S002","KL06S003","KL07M001","KL07M002","KL07S001","KL07S002","KL08M003","KL08M004","KL08S001","KL08S002","KL09M003","KL09M004","KL09P003","KL09P004","KL09C003","KL09C004","KL09B002","KL09B003","KL10P003","KL10P004","KL10C003","KL10C004","KL10B002","KL10B003","KL10M003","KL10M004","MH06M001","MH06S001","MH07M001","MH07S001","MH08M103","MH08S050","MH09M102","MH09M104","MH09S001","MH10M02","MH10M01","MH10S113","MH10S112","MH11B102","MH11M01","MH11M02","MH11P102","MH11C104","MH12B101","MH12C104","MH12C105","MH12P104","MH12M104","MH12M105","RJ10M102","RJ10S012","RJ12M103","RJ12P052","RJ12C053","RJ12B103","TS06M003","TS06S003","TS07M003","TS07S005","TS08S002","TS08S003","TS08M003","TS09M002","TS09P001","TS09S002","TS10M003","TS10S005","TS10S003","JK06M003","JK06S003","JK07M003","JK07S003","JK08M004","JK08S004","JK09M003","JK09S004","JK10M005","JK10S006","EL06M028","EL06M076","EL06S039","EL06S040","EL06S041","EL07M011","EL07M026","EL07S091","EL07S006","EL07P002","EL07S088","EL08M250","EL08M247","EL08B005","EL08S034","EL08P005","EL08S084","EL08S093","EL09B024","EL09C037","EL09C039","EL09P30","EL09P001","EL09M007","EL09M013","EL10M017","EL10M005","EL10B142","EL10B028","EL10C045","EL10C032","EL10P038","EL10B045","EL11M019","EL11B012","EL11P016","EL11C036","EL12M508","EL12M505","EL12B010","EL12C054","EL09C023","EL10C004","EL09P005","EL09P100","EL09C036","NC09M006","EL10C114","EL09M068","EL10P002","EL10P044","EL10C005","EL10C024","EL09B035","EL10B004","EL07M001","EL09C103","EL09B020","EL09P117","EL10C043","EL10C116","EL10B005","EL10M195","EL08S010","EL10P016","EL10C028","EL09B100","EL10B032","NCERT09M01","NC08M001","EL06S088","EL06S066","EL06S006","NCERT09S01","EL07S027","NC06M005","EL09P028","NCERT050","EL06S058","EL07S017","EL06S063","EL09P016","EL09C001","NCERT06M01","EL06M001","EL06S025","EL06S023","EL06S009","EL06S020","EL06S067","EL09S015","EL07S003","NCERT12M01","EL06M032","EL08S001","NC08S003","NCERT12C01","EL06M049","EL06S053","EL06M033","EL10M012","EL08M087","NCERT11C02","NC06S005","EL08M017","EL06M012","NC07S005","EL10M050","NC12B005","EL06S018","EL08M039","NCERT11P01","EL09P003","EL08M003","EL08M011","EL08M038","EL07S105","EL08S007","EL06S084","EL07S019","EL06S014","EL10C022","NCERT12C02","NCERT11C01","NCERT12M02","EB00005","EL08M066","EL09C016","NC12C007","EL07S045","EL06S024","EL06S057","EL10S011","NCERT10M01","EL10M180","EL10M031","EL10M196","EL10P021","EL10P024","EL10C002","EL10B129","EL10P121","EL10C038","NCERT052","EL10B008","EL10B040","EL10B041","EL10C011","EL10B019","EL10P011","NCERT06S01","NC07S001","EL09C120","EL10P013","EL09B101","EL09B002","EL10P003","EL10P004","EL06M005","EL09M022","NCERT12B01","EL09C018","EL07M031","EL02055","EL06M038","EL09S138","EL06S004","EL10S119","EL10P006","EL09S021","NCERT11P02","EL09S106","EL10C001","EL10B013","EL10B102","EL09B033","EL10B006","EL06S038","EL06S022","EL10B018","EL11C029","EL06S235","EL06S054","EL09B113","NCERT12P01","EL06S059","EL07S018","EL06S021","EL07S014","EL07S104","EL10C014","EL06M047","EL06S042","EL07S020","NC12P001","EL06M041","EL06S002","EL07S021","EL08S037","EL07S035","NC09S005","EL08S070","EL08S050","EL07M048","EL07S030","EL07S050","EL08S301","EL07S008","EL07S022","EL07M103","EL07S016","EL07S127","EL07S010","EL08S062","EL07M046","EL08S047","EL07S026","EL08S046","EL08S035","EL11M022","NC11C006","NC11B004","EL12P018","EL11P008","EL12B042","EL09P015","EL09P019","EL09M020","EL06S051","EL06S068","EL08S318","EL09M003","EL07M108","EL08M212","PB06M004","PB06S005","PB07M005","PB07S006","PB08M008","PB08S007","PB09M006","PB09S007","PB10M008","PB10S007","CG06M002","CG06S002","CG07M002","CG07S002","CG08M001","CG08S002","CG09M005","CG09S008","CG10M005","CG10S006","pugneet5187","pugjee5442","pugjee5443","pugneet5186","pugneet5184","pugjee5441","pugneet5188","pugneet5189","pugneet5199","pugjee5444","pugjee5323","pugjee5449","pugneet5183","pugjee5447","pugneet5185","pugneet5182","pugneet5193","pugjee5446","pugneet5191","pugneet5196","elm-22","elm-54","elm-57","pugneet5198","pugjee5439","pugjee5448","pugneet5190","pugjee5434","pugjee5437","pugjee5440","pugjee5445","pugneet5192","pugneet5197","pugjee5379","pugneet296","pugneet298","pugneet299","pugjee5438","pugneet324","pugneet297","pugjee294","pugjee298","pugee037","pugneet5195","pugjee325","iitjee019","pugjee380","pugee025","pugee073","pugee072pv","pugee072cv","pugee072mv","pugee072e","pugee072pb","pugee072cb","pugee072mb","pugee072pa","pugee072ca","pugee072ma","pugee072pt","pugee072ct","pugee072mt","pugee072pm","pugee072cm","pugee072mm","pugcet056","pugee072pae","pugee072cae","pugee072mae","pugee072psj","pugee072csj","pugee072msj","pugee072pkt","pugee072ckt","pugee072mkt","pugjee5330","pugneet5182cmt","pugneet5183cmt","pugneet5184cmt","pugneet5185cmt","pugneet5186cmt","pugneet5187cmt","pugneet5188pmt","pugneet5189pmt","pugneet5190pmt","pugneet5191pmt","pugneet5192pmt","pugneet5193pmt","el12b029","pugneet5194","PUGEE088","PUGEE034","KVPY025","KVPY026","KVPY021","KVPY019","KVPY016","KVPY017","KVPY020","KVPY018","PUGEE072PCK","PUGEE072MCK","PUGEE072CCK","PUGCET030P1","PUGCET031P1","PUGCET032P1","PUGCET033P1"]
    book_codes_in_scope = [book.upper() for book in book_codes_in_scope]
    
    query = {}
    query["content.book_code"] = {"$in":book_codes_in_scope}
    
    projection = {}
    projection["type"] = 1
    projection["content.book_code"] = 1
    projection["name"] = 1
    projection["locales.en.format"] = 1
    
    collection = learning_map_formats_collection()
    cursor = collection.find(query,projection=projection)
    
    book_formats = {}
    i = 0
    for doc in cursor:
        if doc.get("type") == "book":
            i += 1
            book_name,book_code,grade,subject,chapter,topic = doc.get("name"),doc.get("content",{}).get("book_code"),None,None,None,None
            
            for _format in doc.get("locales",{}).get("en",{}).get("format",[]):
                if _format.get("display_name") in ["Grade","grade"]:
                    grade = _format.get("index")
                elif _format.get("display_name") in ["subject","Subject"]:
                    subject = _format.get("index")
                elif _format.get("display_name") in ["Chapter","chapter"]:
                    chapter = _format.get("index")
                elif _format.get("display_name") in ["Topic","topic"]:
                    topic = _format.get("index")
            
            book_formats[doc.get("_id")] = book_name,book_code,grade,subject,chapter,topic
    
    print(i)
    return book_formats


def lmKv_entity(book_formats):
    format_references = [format_reference for format_reference in book_formats]
    
    query = {}
    query["status"] = "active"
    query["format_reference"] = {"$in":list(format_references)}
    
#     print(query)
    projection = {}
    projection["code"] = 1
    projection["_id"] = 1
    projection["format_reference"] = 1
    projection["status"] = 1
    projection["format"] = 1
    
    collection = learning_maps_collection()
    cursor = collection.find(query,projection=projection)
    booklms = {}
    all_kv_entity = []
    
    i = 0
    for doc in cursor:
        i += 1
        code = doc.get("code")
        format_reference = doc.get("format_reference")
        status = doc.get("locales",{}).get("en",{}).get("status")
        
        book_name,book_code,grade_ind,subject_ind,chapter_ind,topic_ind = book_formats.get(format_reference)
        
        grade,subject,chapter,topic,kv_entity = None,None,None,None,None
        for _format in doc.get("format",[]):
            if _format.get("index") == grade_ind:
                grade = _format.get("name")
            elif _format.get("index") == subject_ind:
                subject = _format.get("name")
            elif _format.get("index") == chapter_ind:
                chapter = _format.get("name")
            elif _format.get("index") == topic_ind:
                topic = _format.get("name")
                kv_entity = _format.get("kv_entity_reference")
                all_kv_entity.append(kv_entity)

        booklms[code] = status,book_name,book_code,grade,subject,chapter,topic,kv_entity
    
    all_kv_entity = list(set(all_kv_entity))
    print(i)
    return booklms,all_kv_entity




def getKV_entity_LM(all_kv_entity):
    query = {}
    query["_id"] = {"$in":list(all_kv_entity)}
    
    projection = {}
    projection["_id"] = 1
    projection["content.learning_maps"] = 1
    
    collection = kv_entitycollection()
    cursor = collection.find(query,projection=projection)
    
    lms_kv_entity = {}
    all_lms = []
    
    for doc in cursor:
        _lms = doc.get("content",{}).get("learning_maps",[])
        for lm in _lms:
            if lm in lms_kv_entity:
                lms_kv_entity[lm].add(doc.get("_id"))
            else:
                lms_kv_entity[lm] = set([doc.get("_id")])
                
        all_lms.extend(_lms)
    
    all_lms = list(set(all_lms))
    
    return lms_kv_entity,all_lms


# In[10]:

def main():
    book_formats = getFormatData()
    booklms,all_kv_entity = lmKv_entity(book_formats)

    lms_kv_entity,all_lms = getKV_entity_LM(all_kv_entity)


    # In[11]:


    query = {}
    query["locales.en.status"] = "Published"
    query["type"] = "Video"
    query["content.question_meta_tags.learning_maps"] = {"$in": list(all_lms)}

    projection = {"locales.en.status":1,
                "id":1,"content.question_meta_tags.learning_maps":1,
                 "locales.en.content.augmented_attributes.app_description":1,
                  "locales.en.content.key_attributes.video_type":1,
                 "content.key_attributes.type":1,"locales.en.content.key_attributes.source":1,
                 "locales.en.content.key_attributes.url":1,"content.source_attributes.duration":1,
                 "content.question_meta_tags.primary_concept":1,"content.question_meta_tags.secondary_concept":1,
                 "locales.en.content.custom_attributes.thumbnail":1,
                 "locales.en.content.custom_attributes.bg_thumbnail":1}

    collection = learning_objects_collection()
    cursor = collection.find(query,projection=projection)#.limit(100)

    kv_entity_TopicExplainer = {}
    kv_entityFeatureVideos = {}
    kv_entityConceptsTagged = {}
    kv_entityDesignmateVideos = {}
    videosWithoutPConcepts = []
    dm_videos = set([])

    i = 0
    for doc in cursor:
        i += 1
        
        learning_maps = []
        question_meta_tags = doc.get("content",{}).get("question_meta_tags",[])
    #     print(question_meta_tags)
    #     break
        for question_meta_tag in question_meta_tags:
            learning_maps.extend(question_meta_tag.get("learning_maps",[]))
        
        learning_maps = list(set(learning_maps))
    #     all_lms |= set(learning_maps)
        
        primary_concept = None
        if len(question_meta_tags) > 0:
            primary_concept = question_meta_tags[0].get("primary_concept")
        
        for lm in learning_maps:
            if lm not in lms_kv_entity:
                continue
                
            kv_entities = lms_kv_entity.get(lm)
            for kv_entity in kv_entities:
                if kv_entity in dict(kv_entityConceptsTagged):
                    kv_entityConceptsTagged[kv_entity].add(primary_concept)
                else:
                    kv_entityConceptsTagged[kv_entity] = set([primary_concept])
             
        _type = doc.get("content",{}).get("key_attributes",{}).get("type")
        url = doc.get("locales",{}).get("en",{}).get("content",{}).get("key_attributes",{}).get("url")
        
        if _type == "Topic Explainer":
            for lm in learning_maps:
                if lm not in lms_kv_entity:
                    continue
                
                kv_entities = lms_kv_entity.get(lm)
                for kv_entity in kv_entities:
                    if kv_entity in dict(kv_entity_TopicExplainer):
                        kv_entity_TopicExplainer[kv_entity].add(doc.get("id"))
                    else:
                        kv_entity_TopicExplainer[kv_entity] = set([doc.get("id")])
            
            if "vimeo" in url:
                dm_videos.add(doc.get("id"))
                for lm in learning_maps:
                    if lm not in lms_kv_entity:
                        continue

                    kv_entities = lms_kv_entity.get(lm)
                    for kv_entity in kv_entities:
                        if kv_entity in kv_entityDesignmateVideos:
                            kv_entityDesignmateVideos[kv_entity].add(doc.get("id"))
                        else:
                            kv_entityDesignmateVideos[kv_entity] = set([doc.get("id")])
        else:
    #         print("here")
            for lm in learning_maps:
                if lm not in lms_kv_entity:
                    continue
                
                kv_entities = lms_kv_entity.get(lm)
                for kv_entity in kv_entities:
                    if kv_entity in kv_entityFeatureVideos:
                        kv_entityFeatureVideos[kv_entity].add(doc.get("id"))
                    else:
                        kv_entityFeatureVideos[kv_entity] = set([doc.get("id")])


    print(i)


    # In[12]:


    lm_wise_videos_book = []
    for lm in booklms:
        lmVideoMeta = {}
        status,book_name,book_code,grade,subject,chapter,topic,kv_entity = booklms.get(lm)
        lmVideoMeta["book_name"] = book_name
        lmVideoMeta["book code"] = book_code
        lmVideoMeta["grade"] = grade
        lmVideoMeta["subject"] = subject
        
        lmVideoMeta["chapter"] = chapter
        lmVideoMeta["topic"] = topic
        lmVideoMeta["kv_entity"] = kv_entity
        lmVideoMeta["code"] = lm
        lmVideoMeta["status"] = status
        lmVideoMeta["No. of Topic Explainer Videos"] = 0
        lmVideoMeta["Topic Explainers Videos (Id)"] = ""

        if kv_entity in kv_entity_TopicExplainer:
            lmVideoMeta["No. of Topic Explainer Videos"] = len(kv_entity_TopicExplainer.get(kv_entity))
            lmVideoMeta["Topic Explainers Videos (Id)"] = str(kv_entity_TopicExplainer.get(kv_entity))

        lmVideoMeta["No. of Feature Videos"] = 0
        lmVideoMeta["Feature Videos (Id)"] = ""

        if kv_entity in kv_entityFeatureVideos:
            lmVideoMeta["No. of Feature Videos"] = len(kv_entityFeatureVideos.get(kv_entity))
            lmVideoMeta["Feature Videos (Id)"] = str(kv_entityFeatureVideos.get(kv_entity))

        lmVideoMeta["Number of  Designmate Video Tagged as Topic Explainer"] = 0
        lmVideoMeta["Designmate Videos Tagged as Topic Explainer"] = ""

        if kv_entity in kv_entityDesignmateVideos:
            lmVideoMeta["Number of  Designmate Video Tagged as Topic Explainer"] = len(kv_entityDesignmateVideos.get(kv_entity))
            lmVideoMeta["Designmate Videos Tagged as Topic Explainer"] = str(kv_entityDesignmateVideos.get(kv_entity))

        lmVideoMeta["Concepts tagged to Videos - Comma separated"] = ""

        if kv_entity in kv_entityConceptsTagged:
            lmVideoMeta["Concepts tagged to Videos - Comma separated"] = str(kv_entityConceptsTagged.get(kv_entity))
        
        lm_wise_videos_book.append(lmVideoMeta)
        
    df_book = pd.DataFrame(lm_wise_videos_book)
    # df_book.sort_values(by=['book code','book_name','grade','subject','chapter','topic'],inplace=True,ascending=True)
    df_book.to_csv("BookLMWiseVideoCount.csv",index=False)


    # In[13]:


    df_book["No. of Topic Explainer Videos"] = df_book["No. of Topic Explainer Videos"].astype(str)
    df_book["No. of Feature Videos"] = df_book["No. of Feature Videos"].astype(str)
    df_book["Number of  Designmate Video Tagged as Topic Explainer"] = df_book["Number of  Designmate Video Tagged as Topic Explainer"].astype(str)


    # In[15]:


    df_new = df_book.groupby(["book_name","book code","grade","subject","chapter","topic","kv_entity"]).agg({'code':lambda s: ",".join(s),'No. of Topic Explainer Videos': lambda s: s.unique(),'Topic Explainers Videos (Id)': lambda s: s.unique(),'No. of Feature Videos': lambda s: s.unique(),'Feature Videos (Id)': lambda s: s.unique(),'Number of  Designmate Video Tagged as Topic Explainer': lambda s: s.unique(),'Designmate Videos Tagged as Topic Explainer': lambda s: s.unique(),'Concepts tagged to Videos - Comma separated': lambda s: s.unique()}).reset_index()


    # In[16]:


    df_new.to_csv("BookLMWiseVideoCount.csv",index=False)
    return df_new

i = 0
while True:
    i += 1
    df_new = main()
    update_sheet_by_df("1sjts7ib55h2qxXaYRjjWAa97QY1rgp8Li-OFdy_BX-k",df_new,"Desired output Format for Books")
    print("round ",i," completed")
    time.sleep(82800)
# In[ ]:




