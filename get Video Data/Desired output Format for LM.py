#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pymongo import MongoClient
import pandas as pd
import csv
import os
import time
import openpyxl
import re
from bson.regex import Regex
from bson.son import SON
from update_sheet import *

# In[2]:


mongo_uri = "mongodb://prdcontentgrailembiberuser:AfrogpWiopif=123@10.18.6.18:27017,10.18.6.19:27017,10.18.6.21:27017/admin?authSource=admin&replicaSet=rs0&readPreference=primary&authMechanism=DEFAULT&ssl=false"

def learning_objects_collection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]

    return database["learning_objects"]

def learning_maps_collection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]

    return database["learning_maps"]

def learning_map_formats_collection():
    client = MongoClient(mongo_uri)
    database = client["contentgrail"]

    return database["learning_map_formats"]


# In[3]:


def getLmsData(lms_to_check,in_codes = True):
    query = {}
    if in_codes:
        query["code"] = {"$in":list(lms_to_check)}
    else:
        query["code"] = {"$nin":list(lms_to_check)}
        query["locales.en.status"] = "active"
    
    collection = learning_maps_collection()
    book_formats,lm_formats = getFormatData(list(collection.distinct("format_reference",query)))
    
    projection = {}
    projection["code"] = 1
    projection["_id"] = 1
    projection["format_reference"] = 1
    projection["locales.en.status"] = 1
    projection["locales.en.format"] = 1
    projection["learnpath_name"] = 1

    cursor = collection.find(query,projection=projection)
    booklms = {}
    learnlms = {}
    
    for doc in cursor:
        code = doc.get("code")
        format_reference = doc.get("format_reference")
        status = doc.get("locales",{}).get("en",{}).get("status")
        
        if format_reference in book_formats:
            book_name,book_code,grade_ind,subject_ind,chapter_ind,topic_ind = book_formats.get(format_reference)
            grade,subject,chapter,topic = None,None,None,None
            for _format in doc.get("locales",{}).get("en",{}).get("format",[]):
                if _format.get("index") == grade_ind:
                    grade = _format.get("display_name")
                elif _format.get("index") == subject_ind:
                    subject = _format.get("display_name")
                elif _format.get("index") == chapter_ind:
                    chapter = _format.get("display_name")
                elif _format.get("index") == topic_ind:
                    topic = _format.get("display_name")
            
            booklms[code] = status,book_name,book_code,grade,subject,chapter,topic
        else:
            goal_ind,exam_ind,subject_ind,unit_ind,chapter_ind,topic_ind = lm_formats.get(format_reference)
            goal,exam,subject,unit,chapter,topic = None,None,None,None,None,None
            for _format in doc.get("locales",{}).get("en",{}).get("format",[]):
                if _format.get("index") == goal_ind:
                    goal = _format.get("display_name")
                elif _format.get("index") == exam_ind:
                    exam = _format.get("display_name")
                elif _format.get("index") == subject_ind:
                    subject = _format.get("display_name")
                elif _format.get("index") == unit_ind:
                    unit = _format.get("display_name")
                elif _format.get("index") == chapter_ind:
                    chapter = _format.get("display_name")
                elif _format.get("index") == topic_ind:
                    topic = _format.get("display_name")
            
            learnlms[code] = status,goal,exam,subject,unit,chapter,topic,doc.get("learnpath_name")
                
    return booklms,learnlms


# In[4]:


def getFormatData(format_refs):
    query = {}
    query["_id"] = {"$in":format_refs}
    
    projection = {}
    projection["type"] = 1
    projection["code"] = 1
    projection["name"] = 1
    projection["locales.en.format"] = 1
    
    collection = learning_map_formats_collection()
    cursor = collection.find(query,projection=projection)
    
    book_formats = {}
    lm_formats = {}
    for doc in cursor:
        if doc.get("type") == "book":
            book_name,book_code,grade,subject,chapter,topic = doc.get("name"),doc.get("code"),None,None,None,None
            
            for _format in doc.get("locales",{}).get("en",{}).get("format",[]):
                if _format.get("display_name") in ["Grade","grade"]:
                    grade = _format.get("index")
                elif _format.get("display_name") in ["subject","Subject"]:
                    subject = _format.get("index")
                elif _format.get("display_name") in ["Chapter","chapter"]:
                    chapter = _format.get("index")
                elif _format.get("display_name") in ["Topic","topic"]:
                    topic = _format.get("index")
            
            book_formats[doc.get("_id")] = book_name,book_code,grade,subject,chapter,topic
        else:
            goal,exam,subject,unit,chapter,topic = None,None,None,None,None,None
            
            for _format in doc.get("locales",{}).get("en",{}).get("format",[]):
                if _format.get("display_name") in ["Goal","goal"]:
                    goal = _format.get("index")
                elif _format.get("display_name") in ["Exam","Exam"]:
                    exam = _format.get("index")
                elif _format.get("display_name") in ["subject","Subject"]:
                    subject = _format.get("index")
                elif _format.get("display_name") in ["Unit","unit"]:
                    unit = _format.get("index")
                elif _format.get("display_name") in ["Chapter","chapter"]:
                    chapter = _format.get("index")
                elif _format.get("display_name") in ["Topic","topic"]:
                    topic = _format.get("index")
            
            lm_formats[doc.get("_id")] = goal,exam,subject,unit,chapter,topic

    return book_formats,lm_formats


# In[5]:

def main():
    query = {}
    query["locales.en.status"] = "Published"
    query["type"] = "Video"

    projection = {"id":1,"content.question_meta_tags.learning_maps":1,
                 "locales.en.content.augmented_attributes.app_description":1,
                  "locales.en.content.key_attributes.video_type":1,
                 "content.key_attributes.type":1,"locales.en.content.key_attributes.source":1,
                 "locales.en.content.key_attributes.url":1,"content.source_attributes.duration":1,
                 "content.question_meta_tags.primary_concept":1,"content.question_meta_tags.secondary_concept":1,
                 "locales.en.content.custom_attributes.thumbnail":1,
                 "locales.en.content.custom_attributes.bg_thumbnail":1}

    collection = learning_objects_collection()
    cursor = collection.find(query,projection=projection)#.limit(1000)

    lm_TopicExplainer = {}
    lmFeatureVideos = {}
    lmConceptsTagged = {}
    lmDesignmateVideos = {}
    videosWithoutPConcepts = []
    all_lms = set([])
    dm_videos = set([])
    i = 0
    for doc in cursor:
        i += 1
        
        learning_maps = []
        question_meta_tags = doc.get("content",{}).get("question_meta_tags",[])
    #     print(question_meta_tags)
    #     break
        for question_meta_tag in question_meta_tags:
            learning_maps.extend(question_meta_tag.get("learning_maps",[]))
        
        learning_maps = list(set(learning_maps))
        all_lms |= set(learning_maps)
        
        primary_concept = None
        if len(question_meta_tags) > 0:
            primary_concept = question_meta_tags[0].get("primary_concept")
        
            
        for lm in learning_maps:
            if lm in lmConceptsTagged:
                lmConceptsTagged[lm].add(primary_concept)
            else:
                lmConceptsTagged[lm] = set([primary_concept])
             
        _type = doc.get("content",{}).get("key_attributes",{}).get("type")
        url = doc.get("locales",{}).get("en",{}).get("content",{}).get("key_attributes",{}).get("url")
        
        if _type == "Topic Explainer":
            for lm in learning_maps:
                if lm in lm_TopicExplainer:
                    lm_TopicExplainer[lm].add(doc.get("id"))
                else:
                    lm_TopicExplainer[lm] = set([doc.get("id")])
            
            if "vimeo" in url:
                dm_videos.add(doc.get("id"))
                for lm in learning_maps:
                    if lm in lmDesignmateVideos:
                        lmDesignmateVideos[lm].add(doc.get("id"))
                    else:
                        lmDesignmateVideos[lm] = set([doc.get("id")])
        else:
            for lm in learning_maps:
                if lm in lmFeatureVideos:
                    lmFeatureVideos[lm].add(doc.get("id"))
                else:
                    lmFeatureVideos[lm] = set([doc.get("id")])


    print(i)


    # In[13]:


    len(all_lms)


    # In[7]:


    booklms,learnlms = getLmsData(all_lms)


    # In[10]:


    len(learnlms)


    # In[12]:


    for l in learnlms:
        print(learnlms[l])
        break


    # In[14]:


    lm_wise_videos_book = []
    lm_wise_videos_learn = []
    for lm in all_lms:
        lmVideoMeta = {}
        status = None
        
        if lm in learnlms:
            status,goal,exam,subject,unit,chapter,topic,learnpath_name = learnlms.get(lm)
            lmVideoMeta["Goal"] = goal
            lmVideoMeta["Exam"] = exam
            lmVideoMeta["Subject"] = subject
            lmVideoMeta["Unit"] = unit
            
            lmVideoMeta["chapter"] = chapter
            lmVideoMeta["topic"] = topic
            lmVideoMeta["code"] = lm
            lmVideoMeta["learnpath_name"] = learnpath_name
            lmVideoMeta["status"] = status
            lmVideoMeta["No. of Topic Explainer Videos"] = 0
            lmVideoMeta["Topic Explainers Videos (Id)"] = ""

            if lm in lm_TopicExplainer:
                lmVideoMeta["No. of Topic Explainer Videos"] = len(lm_TopicExplainer.get(lm))
                lmVideoMeta["Topic Explainers Videos (Id)"] = str(lm_TopicExplainer.get(lm))

            lmVideoMeta["No. of Feature Videos"] = 0
            lmVideoMeta["Feature Videos (Id)"] = ""

            if lm in lmFeatureVideos:
                lmVideoMeta["No. of Feature Videos"] = len(lmFeatureVideos.get(lm))
                lmVideoMeta["Feature Videos (Id)"] = str(lmFeatureVideos.get(lm))

            lmVideoMeta["Number of  Designmate Video Tagged as Topic Explainer"] = 0
            lmVideoMeta["Designmate Videos Tagged as Topic Explainer"] = ""

            if lm in lmDesignmateVideos:
                lmVideoMeta["Number of  Designmate Video Tagged as Topic Explainer"] = len(lmDesignmateVideos.get(lm))
                lmVideoMeta["Designmate Videos Tagged as Topic Explainer"] = str(lmDesignmateVideos.get(lm))

            lmVideoMeta["Concepts tagged to Videos - Comma separated"] = ""

            if lm in lmConceptsTagged:
                lmVideoMeta["Concepts tagged to Videos - Comma separated"] = str(lmConceptsTagged.get(lm))
            
        #     if lm in booklms:
        #         lm_wise_videos_book.append(lmVideoMeta)
        #     else:
            lm_wise_videos_learn.append(lmVideoMeta)

    # df_book = pd.DataFrame(lm_wise_videos_book)
    df_learn = pd.DataFrame(lm_wise_videos_learn)

    df_learn.to_csv("lmWiseVideoCount.csv",index=False)

    return df_learn
    # df_book.to_csv("BookLMWiseVideoCount.csv",index=False)

i = 0
while True:
    i += 1
    print("bhbh")
    df_new = main()
    update_sheet_by_df("1L8FOMNULq6KUeRL7GsAraKBdvc2oq4Fo9tdukjAzW_E",df_new,"Desired output Format for LM")

    print("round ",i," completed")
    time.sleep(82800)
