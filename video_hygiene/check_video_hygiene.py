import requests
from sheet_util import *
import pandas as pd
import json
import openpyxl
import os
from combine_csvs import combine_csvs

def get_goals_exams():
	df = get_sheet_to_df("1SfUF0PA_MEq749LulJZE4qhx3JM8qLskKM3WARGjTps","Sheet3")
	df.to_csv("goals_and_exams_in_scope_for_fiber_app.csv",index=False)
	return df

if __name__ == '__main__':
	grades = ["1","2","3","4","5","6","7","8","9","10","11","12","PreUG","PrePG"]
	os.system("sudo rm -r Temp_Results")
	os.system("mkdir Temp_Results")

	os.system("sudo rm -r Results")
	os.system("mkdir Results")

	for grade in grades:
		df_results = pd.DataFrame(columns=["Grade","Video Id","Duration","Embiums","Status","Thumbnail","Bg_thumnail"])
		url = f"http://content-demo.embibe.com/fiber/videos/{grade}"

		payload={}
		headers = {
		  'accept': 'application/json',
		  'Authorization': '048f38be-1b07-4b21-8f24-eac727dce217:gSEkC3dqDcIv1bbOk78UD9owjn7ins8D',
		  'Cookie': 'V_ID=ultimate.2020-12-16.606a61d1765b4795fe927b40c1c1243d; embibe-refresh-token=793d37b1-73d2-495d-b137-395c2305a94a'
		}

		response = None
		for i in range(3):
			response = requests.request("GET", url, headers=headers, data=payload)

			if response.status_code == 200 and response.json() != None:
				break

		if response.status_code == 200 and response.json() != None:
			for _id in response.json():
				for video in _id["details"]:
					video_id = video["video_id"]

					duration = None
					if "duration" in video:
						duration = video["duration"]

					embiums = None
					if "embiums" in video:
						embiums = video["embiums"]

					status = None
					if "status" in video:
						status = video["status"]

					thumbnail = None
					if "thumbnail" in video:
						thumbnail = video["thumbnail"]

					bg_thumbnail = None
					if "bg_thumbnail" in video:
						bg_thumbnail = video["bg_thumbnail"]

					df_results.loc[len(df_results)] = [grade,video_id,duration,embiums,status,thumbnail,bg_thumbnail]


		df_results.to_excel(f"Temp_Results/video_hygiene_results_{grade}.xlsx",index=False)

	combine_csvs()
