import glob
import os
import pandas as pd
import ast
import openpyxl

def combine_csvs():
	for csv_name in ["video_hygiene_results"]:

		try:
			csv_list = glob.glob(f"Temp_Results/{csv_name}_*.xlsx")
			df = pd.concat([pd.read_excel(x) for x in csv_list]).drop_duplicates()

			df.to_excel(f"Results/{csv_name}.xlsx",index=False)
		except Exception as e:
			pass
		


