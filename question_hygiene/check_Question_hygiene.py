# Requires pymongo 3.6.0+
from pymongo import MongoClient
import pandas as pd
import csv
import os
import time
import openpyxl

client = MongoClient("mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.78/contentgrail?authSource=contentgrail&replicaSet=cg-mongo-prod&readPreference=secondaryPreferred")
# client = MongoClient("mongodb://10.143.1.95:27017/")
database = client["contentgrail"]
collection = database["learning_objects"]
concept_collection = database["concepts"]
lm_collection = database["learning_maps"]


def get_lms(learning_map_codes : list) -> dict:
	lms = {}
	lm_cursor = lm_collection.find({'code': {'$in': learning_map_codes}},
									{'code': 1, 'status': 1})

	for doc in list(lm_cursor):
		lms[doc['code']] = str(doc.get('status','status key not found'))

	lm_cursor.close()
	return lms

def get_concepts(concept_codes):
	concepts = {}
	concepts_cursor = concept_collection.find({'code': {'$in': concept_codes}},
									{'code': 1, 'status': 1})

	for doc in list(concepts_cursor):
		concepts[doc['code']] = doc.get('status','status key not found')

	concepts_cursor.close()
	return concepts

def get_question_hygiene():
	query = {}
	query["type"] = "Question"
	query["status"] = "Published"

	projection = {}
	projection["id"] = 1.0
	projection["question_code"] = 1.0
	projection["title"] = 1.0
	projection["purpose"] = 1.0
	projection["content.question_details.en.answers"] = 1.0
	projection["content.question_meta_tags.ideal_time"] = 1.0
	projection["content.question_meta_tags.bloom_level"] = 1.0
	projection["content.question_meta_tags.primary_concept"] = 1.0
	projection["content.question_meta_tags.difficulty_level"] = 1.0
	projection["content.question_meta_tags.secondary_concept"] = 1.0
	projection["content.question_meta_tags.skill"] = 1.0
	projection["content.question_meta_tags.learning_maps"] = 1.0

	# projection["content.question_meta_tags.bloom_level"] = 1.0

	cursor = collection.find(query, projection = projection).limit(20000)
	questions = []
	try:
		# print(cursor)
		all_learning_maps = []
		all_primary_concepts = []
		for doc in cursor:
			# print("\t",doc['id'])
			try:
				question_meta_data = {}
				question_meta_data['id'] = doc['id']
				question_meta_data['question_code'] = doc['question_code']
				question_meta_data['title'] = doc['title']

				question_meta_data['purpose'] = str(set(doc.get('purpose',[])))[1:-1]
				if question_meta_data['purpose'] == "et(":
					question_meta_data['purpose'] = ""
					
				question_meta_data['Exactly one correct answer available'] = "No"
				answers = doc.get("content",{}).get("question_details",{}).get("en",{}).get("answers",[])
				if answers == []:
					question_meta_data['Exactly one correct answer available'] = "Answers not found"
				else:
					answer_flag = False
					for answer in answers:
						if answer.get("is_correct","is_correct key not found in one/more answers") == False:
							continue

						if answer.get("is_correct","is_correct key not found in one/more answers") == True:
							answer_flag = True
							continue

						question_meta_data['Exactly one correct answer available'] = "is_correct key not found in one/more answers"
						break

					question_meta_data['Exactly one correct answer available'] = "Yes"

				question_meta_tags = doc.get("content",{}).get("question_meta_tags",[])
				if len(question_meta_tags) == 0:
					question_meta_data["ideal_time"] = "question_meta_tags key not available"
					question_meta_data["bloom_level"] = "question_meta_tags key not available"
					question_meta_data["primary_concept"] = "question_meta_tags key not available"
					question_meta_data["difficulty_level"] = "question_meta_tags key not available"
					question_meta_data["secondary_concept"] = "question_meta_tags key not available"
					question_meta_data["skill"] = "question_meta_tags key not available"
					question_meta_data["learning_maps"] = "question_meta_tags key not available"
				else:
					question_meta_tags = question_meta_tags[0]
					question_meta_data["ideal_time"] = question_meta_tags.get("ideal_time","Key not found")
					question_meta_data["bloom_level"] = question_meta_tags.get("bloom_level","Key not found")
					primary_concept = question_meta_tags.get("primary_concept","Key not found")
					question_meta_data["primary_concept"] = primary_concept
					question_meta_data["difficulty_level"] = question_meta_tags.get("difficulty_level","Key not found")
					question_meta_data["secondary_concept"] = str(question_meta_tags.get("secondary_concept",[]))[1:-1]
					question_meta_data["skill"] = question_meta_tags.get("skill","Key not found")
					learning_maps = question_meta_tags.get("learning_maps",[])
					question_meta_data["learning_maps"] = str(learning_maps)[1:-1]
					all_learning_maps.extend(learning_maps)
					all_primary_concepts.append(primary_concept)

					all_learning_maps = list(set(all_learning_maps))
					all_primary_concepts = list(set(all_primary_concepts))

				questions.append(question_meta_data)
			except Exception as e:
				print(doc['question_code'])
				print(e)

		df = pd.DataFrame(questions)
		lms_status = get_lms(all_learning_maps)
		concept_status = get_concepts(all_primary_concepts)
		# print(lms_status)
		# print("\n\n\n",concept_status)
		df["primary_concept status"] = [""]*len(df)
		df["learning_maps status"] = [""]*len(df)
		df["All learning_maps active"] = [""]*len(df)

		for ind in df.index:
			primary_concept = df["primary_concept"][ind]
			df["primary_concept status"][ind] = concept_status.get(primary_concept,"Not found in concepts collection")

			# print(primary_concept)
			lms = str(df["learning_maps"][ind]).split(",")
			learning_maps_status = ""
			all_lms_active = "Yes"
			for lm in lms:
				lm_status = lms_status.get(str(lm).replace(" ","").replace("'",""),"Not found in learning_maps collection")
				learning_maps_status += str(lm) + ": " + lm_status + ", "

				# print(str(lm).replace(" ","").replace("'",""),": ",lm_status)
				if lm_status != "active":
					all_lms_active = "No"


			df["learning_maps status"][ind] = learning_maps_status
			df["All learning_maps active"][ind] = all_lms_active

		df.to_csv("Results/question_hygiene_check_results.csv",index=False)
			
	finally:
		# print("Failed")
		client.close()


if os.path.exists('Results'):
	os.system("rm -r Results")

os.system("mkdir Results")

# time.sleep(8000)
get_question_hygiene()
