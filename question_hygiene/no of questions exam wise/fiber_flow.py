import os
import csv
import json
import string
import random
import requests
import sys
from get_no_of_ques import *
import traceback
from update_sheet import update_sheet
import time

class Source(object):
	def __init__(self):
		super(Source, self).__init__()
		self.headers = {
			'Connection': 'keep-alive',
			'Accept': '*/*',
			'Authorization': '048f38be-1b07-4b21-8f24-eac727dce217:gSEkC3dqDcIv1bbOk78UD9owjn7ins8D',
			'Content-Type': 'application/json',
		}
		self.exams = None
		self.chapters_in_seq = []
		self.topics_in_chapter = {}
		self.chapters_with_seq = {}
		self.topics_with_seq = {}
		self.host = 'https://preprodms.embibe.com'
		self.seq_response = None

	def get_all_exams(self):
		df = pd.read_csv("goals_and_exams_in_scope_for_fiber_app.csv")

		exams = []
		for ind in df.index:
			exams.append(str(df['Exam in backend'][ind]))

		self.exams = exams

	def callAPI2(self, url, payload, method):
		response = None

		for i in range(5):
			response = requests.request(method, url, headers=self.headers, data=payload)
			if response.status_code == 200 and response.json() != None:
				return response

		return response

	def change_end_point(self,end_point):
		ans = ""
		for s in end_point:
			if s == '?':
				ans += '%3F'
			else:
				ans += s

		return ans

	def get_all_subjects(self,_grade,_exam,end_point,url):

		end_point = self.change_end_point(end_point)
		
		response1 = self.callAPI2(url+end_point,"{}",'GET')

		# print(self.exams)

		if 'Exam' in response1.json():
			
			for exam in response1.json()['Exam']:
				if _exam != exam['name']:
					continue

				# print("\n\tExam: ",exam['name'])
				
				try:
					return self.get_all_subjects(_grade,_exam,exam['name'],url+end_point+"/")
				except Exception as e:
					print(traceback.format_exc())

		elif 'Subject' in response1.json():
			all_units = []
			
			for subject in response1.json()["Subject"]:
				if str(_grade) in ["1","2","3","4","5","6","7","8","9","10"] and subject['name'] not in ["Mathematics","Environmental Studies","गणित"]:
					try:
						all_units.extend([subject['name'] + "--" + x for x in self.get_all_subjects(_grade,_exam,subject['name'],url+end_point+"/")])
					except Exception as e:
						print(traceback.format_exc())
				else:
					all_units.extend([subject['name']])
				
			return all_units

		else:
			for key in response1.json():
				units = []
				for unit in response1.json()[key]:
					# print("\t\t\t",unit['name'])
					units.append(unit['name'])

				return units


def fiber_flow():
	src = Source()

	df = pd.read_csv("goals_and_exams_in_scope_for_fiber_app.csv")
	os.system("sudo rm -r Results")
	os.system("sudo mkdir Results")

	goal_exams = []
	all_status = [
			"Approved",
			"Published",
			"Published UAT Accepted",
			"UAT Accepted",
			"UAT Rejected"
		]

	df_results = pd.DataFrame(columns=["Format_refrence","Goal","Exam","Subject","Unit","Grade","No of LMs tagged"])
	for status in all_status:
		df_results[status] = ""

	i = 0
	questions_covered = {}
	for status in all_status:
		questions_covered[status] = []
		
	for ind in df.index:
		exam = df["Exam in backend"][ind]
		goal = df["Goal in backend"][ind]

		goal_exam = goal + "--" + exam

		if goal_exam in goal_exams:# or exam not in ["6th CBSE","7th CBSE"]:
			continue

		goal_exams.append(goal_exam)
		print(goal_exam)
		grade = df["grade"][ind]
		stream = ""
		if str(grade) in ["1","2","3","4","5","6","7","8","9","10"]:
			stream = "1st_10th"
		elif str(grade) in ["11","12"]:
			stream = "11th_12th"
		else:
			stream = str(grade)

		format_refrence = df["Format_refrence"][ind]

		all_units = src.get_all_subjects(grade,exam,goal,f'http://content-demo.embibe.com/fiber_app/learning_maps/filters/{format_refrence}/')
		if all_units == None:
			df_results.loc[len(df_results)] = [format_refrence,goal,exam,"Exam inactive in CG","Exam inactive in CG",grade] + ["Exam inactive in CG"]*(len(all_status) + 1)
		else:
			for unit in all_units:

				row_no = len(df_results)
				lms = get_active_lms(str(goal + "--" + exam + "--" + unit).lower())
				# print("Len Lms:",len(lms))
				try:
					unit_name = unit.split("--")[1]
				except Exception as e:
					unit_name = ""
				
				df_results.loc[len(df_results)] = [format_refrence,goal,exam,unit.split("--")[0],unit_name,grade,len(lms)] + [""]*len(all_status)
				
				for status in all_status:
					df_results[status][row_no],questions = get_question_hygiene(status,lms,questions_covered[status],unit,stream,grade)
					questions_covered[status].extend(questions)
			
			df_results.to_csv("Results/results.csv",index=False)

	# update_sheet()


if __name__ == '__main__':
	while  True:
		fiber_flow()
		break
		time.sleep(54000)


