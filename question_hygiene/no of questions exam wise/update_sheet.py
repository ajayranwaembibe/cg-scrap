import gspread
from oauth2client.service_account import ServiceAccountCredentials
import pandas as pd
import csv
import gspread_dataframe as gd
import time
import glob
import traceback

# use creds to create a client to interact with the Google Drive API
scope = ['https://spreadsheets.google.com/feeds',
		'https://www.googleapis.com/auth/drive']

creds = ServiceAccountCredentials.from_json_keyfile_name('apiautomation-5f15e583dede.json', scope)
client = gspread.authorize(creds)

gc = gspread.service_account(filename='apiautomation-5f15e583dede.json')

def get_all_worksheets(file_id):
	for i in range(3):
		try:
			sheet = gc.open_by_key(file_id)
			worksheets = sheet.worksheets()
			worksheet_names = []
			for wk in worksheets:
				worksheet_names.append(wk.title)

			return worksheet_names

		except Exception as e:
			print(traceback.format_exc())
			time.sleep(150)
			print(e)

	return []

def add_worksheet(file_id,wk_name):
	sheet = gc.open_by_key(file_id)
	worksheet = sheet.add_worksheet(wk_name,100,1)

def add_worksheet_with_df(file_id,df,wk_name):
	for i in range(3):
		try:
			sheet = gc.open_by_key(file_id)
			worksheet = sheet.add_worksheet(wk_name,100,1)

			update_sheet_by_df(file_id,df,wk_name)

			return

		except Exception as e:
			print(traceback.format_exc())
			time.sleep(150)
			print(e)

def update_sheet_by_df(file_id,df,wk_name):

	# print("\tGetting google sheet")
	for i in range(3):
		try:
			sheet = gc.open_by_key(file_id)
			worksheet = sheet.worksheet(wk_name)

			# updated = existing.append(existing)
			# print("\tUpdating ",wk_name," sheet")
			worksheet.clear()
			gd.set_with_dataframe(worksheet, df)
			return

		except Exception as e:
			print(traceback.format_exc())
			time.sleep(150)
			print(e)
	
def get_sheet_to_df(file_id,worksheet_name):
	for i in range(3):
		try:
			sheet = gc.open_by_key(file_id)
			# print("Getting sheet")
			worksheet = sheet.worksheet(worksheet_name)

			# print("Success: Genrating DataFrame")
			return pd.DataFrame(worksheet.get_all_records())

		except Exception as e:
			print(traceback.format_exc())
			time.sleep(150)
			print(e)
		

def update_sheet():
	all_csvs = glob.glob("Results/*.csv")
	all_csvs = [csv.replace("Results/","").replace(".csv","") for csv in all_csvs]
	file_id = "1Xlk0yCYDhNB7FBSblWcSRV5E6R7hp5A6QMWkvQQomL0"
	
	all_worksheets = get_all_worksheets(file_id)
	for csv in all_csvs:
		print(csv)
		df = pd.read_csv(f"Results/{csv}.csv")
		if csv in all_worksheets:
			update_sheet_by_df(file_id,df,csv)
		else:
			add_worksheet_with_df(file_id,df,csv)

	
def get_true_data():
	active_smes_df = get_sheet_to_df('1d3Mavf-LP3RcL03jPgWuq3pwCxsrNjjJJAxnkvby1Xg','Active_SMEs')
	active_smes_df.to_csv('Results/active_smes.csv')



