# Requires pymongo 3.6.0+
from pymongo import MongoClient
import pandas as pd
import csv
import os
import time
import openpyxl
import re
import glob

client = MongoClient("mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.78/contentgrail?authSource=contentgrail&replicaSet=cg-mongo-prod&readPreference=secondaryPreferred")
# client = MongoClient("mongodb://10.143.1.95:27017/")
database = client["contentgrail"]
collection = database["learning_objects"]
concept_collection = database["concepts"]
lm_collection = database["learning_maps"]


def get_lms(learning_map_codes : list) -> dict:
	lms = {}
	lm_cursor = lm_collection.find({'code': {'$in': learning_map_codes}},
									{'code': 1, 'status': 1})

	for doc in list(lm_cursor):
		lms[doc['code']] = str(doc.get('status','status key not found'))

	lm_cursor.close()
	return lms


def get_active_lms(goal_exam_sub):
	# print("get_active_lms")
	# active_goal_exams = ['cbse--1st cbse','cbse--2nd cbse','cbse--3rd cbse','cbse--4th cbse','cbse--5th cbse','cbse--6th cbse','cbse--7th cbse','cbse--8th cbse','cbse--9th cbse','cbse--10th cbse','cbse--11th cbse','cbse--12th cbse','icse--1st icse','icse--2nd icse','icse--3rd icse','icse--4th icse','icse--5th icse','icse--6th icse','icse--7th icse','icse--8th icse','icse--9th icse','icse--10th icse','icse--11th icse','icse--12th icse','tamil nadu board--1st tamil nadu state board','tamil nadu board--2nd tamil nadu state board','tamil nadu board--3rd tamil nadu state board','tamil nadu board--4th tamil nadu state board','tamil nadu board--5th tamil nadu state board','tamil nadu board--6th tamil nadu state board','tamil nadu board--7th tamil nadu state board','tamil nadu board--8th tamil nadu state board','tamil nadu board--9th tamil nadu state board','tamil nadu board--10th tamil nadu state board','tamil nadu board--11th tamil nadu state board','tamil nadu board--12th tamil nadu state board','maharashtra board--1st maharashtra board','maharashtra board--2nd maharashtra board','maharashtra board--3rd maharashtra board','maharashtra board--4th maharashtra board','maharashtra board--5th maharashtra board','maharashtra board--6th maharashtra board','maharashtra board--7th maharashtra board','maharashtra board--8th maharashtra board','maharashtra board--9th maharashtra board','maharashtra board--10th maharashtra board','maharashtra board--11th maharashtra board','maharashtra board--12th maharashtra board','rajasthan board--1st rbse','rajasthan board--2nd rbse','rajasthan board--3rd rbse','rajasthan board--4th rbse','rajasthan board--5th rbse','rajasthan board--6th rbse','rajasthan board--7th rbse','rajasthan board--8th rbse','rajasthan board--9th rbse','rajasthan board--10th rbse','rajasthan board--11th rbse','rajasthan board--12th rbse','engineering--jee main','engineering--jee advanced','engineering--mht-cet','engineering--bitsat','engineering--ap eamcet','engineering--viteee','engineering--viteee (with biology)','engineering--kcet (ug)','engineering--srmjeee (ug)','engineering--bitsat (with biology)','engineering--amu-at (b.tech.)','engineering--ts eamcet','medical--ap eamcet','medical--neet','medical--ts eamcet','banking--bihar cooperative bank assistant prelims','banking--rbi assistant prelims','banking--ibps po mains','banking--ibps rrb officer scale-i prelims','banking--ibps clerk prelims','banking--ibps clerk mains','banking--ibps rrb office assistant mains','banking--ibps rrb office assistant prelims','banking--ibps po prelims','banking--sbi po mains','banking--ibps rrb officer scale-i mains','banking--sbi clerk prelims','banking--sbi clerk mains','banking--sbi po prelims','insurance--lic aao prelims','insurance--niacl assistant prelims','insurance--lic assistant prelims','insurance--insurance','railways--rpf sub inspector cbt 1','railways--rpf constable cbt 1','railways--rrc group d cbt','railways--rrb ntpc cbt 1','railways--railways','ssc--ib security assistant or executive tier 1','ssc--ssc stenographer grade c and d','ssc--ssc cpo paper-i','ssc--delhi police constable cbt','ssc--ssc mts tier i','ssc--ssc chsl tier-i','ssc--ssc cgl tier-i','ssc--ssc','teaching--ctet paper 1','teaching--ctet paper 2','defence--drdo mts tier i','defence--cisf head constable','defence--defence','jammu and kashmir board--1st jammu and kashmir board','jammu and kashmir board--2nd jammu and kashmir board','jammu and kashmir board--3rd jammu and kashmir board','jammu and kashmir board--4th jammu and kashmir board','jammu and kashmir board--5th jammu and kashmir board','jammu and kashmir board--6th jammu and kashmir board','jammu and kashmir board--7th jammu and kashmir board','jammu and kashmir board--8th jammu and kashmir board','jammu and kashmir board--9th jammu and kashmir board','jammu and kashmir board--10th jammu and kashmir board','jammu and kashmir board--11th jammu and kashmir board','jammu and kashmir board--12th jammu and kashmir board','national recruitment agency--nra cet - graduate level','national recruitment agency--nra cet - higher secondary level','national recruitment agency--nra cet - matriculation level','telangana board--1st telangana state board','telangana board--2nd telangana state board','telangana board--3rd telangana state board','telangana board--4th telangana state board','telangana board--5th telangana state board','telangana board--6th telangana state board','telangana board--7th telangana state board','telangana board--8th telangana state board','telangana board--9th telangana state board','telangana board--10th telangana state board','telangana board--11th telangana state board','telangana board--12th telangana state board','karnataka board--1st karnataka state board','karnataka board--2nd karnataka state board','karnataka board--3rd karnataka state board','karnataka board--4th karnataka state board','karnataka board--5th karnataka state board','karnataka board--6th karnataka state board','karnataka board--7th karnataka state board','karnataka board--8th karnataka state board','karnataka board--9th karnataka state board','karnataka board--10th karnataka state board','karnataka board--11th karnataka state board','karnataka board--12th karnataka state board','kerala board--1st kerala board','kerala board--2nd kerala board','kerala board--3rd kerala board','kerala board--4th kerala board','kerala board--5th kerala board','kerala board--6th kerala board','kerala board--7th kerala board','kerala board--8th kerala board','kerala board--9th kerala board','kerala board--10th kerala board','kerala board--11th kerala board','kerala board--12th kerala board','uttar pradesh board--9th uttar pradesh board','uttar pradesh board--10th uttar pradesh board','uttar pradesh board--11th uttar pradesh board','uttar pradesh board--12th uttar pradesh board']
	# active_goal_exams = ['cbse--10th cbse','engineering--bitsat']
	
	cursor = lm_collection.find({'status':'active','learnpath_name':{"$regex":goal_exam_sub}})
	lms = []

	for doc in cursor:
		lms.append(doc["code"])

	client.close()
		# break

	return lms

def get_question_hygiene(_status,_lms,questions,unit,stream,grade):
	query = {}
	query["type"] = "Question"
	query["status"] = _status
	query["id"] = {"$nin":questions}
	query["content.question_meta_tags.learning_maps"] = {"$in":_lms}

	projection = {}
	projection["id"] = 1.0
	projection["question_code"] = 1.0

	# print("get_question_hygiene")
	_questions = collection.distinct("id",query)#.limit(1000)
	_questions = list(_questions)

	try:
		# print(cursor)
		all_learning_maps = []
		all_primary_concepts = []

		if _status == "Approved":
			all_csvs = glob.glob("Results/*.csv")
			df = None
			if f"Results/{unit}_{stream}.csv" in all_csvs:
				df = pd.read_csv(f"Results/{unit}_{stream}.csv")
			else:
				df = pd.DataFrame(columns=["Id"])

			questions = df["Id"].to_list()
			prev_ques = len(df)

			questions.extend(_questions)

			questions = list(set(questions))
			df = pd.DataFrame(columns=["Id"])
			df["Id"] = questions
			df.to_csv(f"Results/{unit}_{stream}.csv",index=False)

			# return len(df)-prev_ques,_questions

		return len(_questions),_questions
			
	finally:
		# print("Failed")
		client.close()


# lms = get_active_lms("cbse--10th cbse--science")
# get_question_hygiene("",lms)
