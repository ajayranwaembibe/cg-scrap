# Requires pymongo 3.6.0+
from pymongo import MongoClient
import pandas as pd
import csv
import os
import time
import openpyxl
import re

client = MongoClient("mongodb://ro_dsl:EHJpUwVO2vgMuk@10.141.11.78/contentgrail?authSource=contentgrail&replicaSet=cg-mongo-prod&readPreference=secondaryPreferred")
# client = MongoClient("mongodb://10.143.1.95:27017/")
database = client["contentgrail"]
collection = database["learning_objects"]
concept_collection = database["concepts"]
lm_collection = database["learning_maps"]


def get_lms(learning_map_codes : list) -> dict:
	lms = {}
	lm_cursor = lm_collection.find({'code': {'$in': learning_map_codes}},
									{'code': 1, 'status': 1})

	for doc in list(lm_cursor):
		lms[doc['code']] = str(doc.get('status','status key not found'))

	lm_cursor.close()
	return lms

def get_concepts(concept_codes):
	concepts = {}
	concepts_cursor = concept_collection.find({'code': {'$in': concept_codes}},
									{'code': 1, 'status': 1})

	for doc in list(concepts_cursor):
		concepts[doc['code']] = doc.get('status','status key not found')

	concepts_cursor.close()
	return concepts

def get_active_lms():
	print("get_active_lms")
	active_goal_exams = ['cbse--1st cbse','cbse--2nd cbse','cbse--3rd cbse','cbse--4th cbse','cbse--5th cbse','cbse--6th cbse','cbse--7th cbse','cbse--8th cbse','cbse--9th cbse','cbse--10th cbse','cbse--11th cbse','cbse--12th cbse','icse--1st icse','icse--2nd icse','icse--3rd icse','icse--4th icse','icse--5th icse','icse--6th icse','icse--7th icse','icse--8th icse','icse--9th icse','icse--10th icse','icse--11th icse','icse--12th icse','tamil nadu board--1st tamil nadu state board','tamil nadu board--2nd tamil nadu state board','tamil nadu board--3rd tamil nadu state board','tamil nadu board--4th tamil nadu state board','tamil nadu board--5th tamil nadu state board','tamil nadu board--6th tamil nadu state board','tamil nadu board--7th tamil nadu state board','tamil nadu board--8th tamil nadu state board','tamil nadu board--9th tamil nadu state board','tamil nadu board--10th tamil nadu state board','tamil nadu board--11th tamil nadu state board','tamil nadu board--12th tamil nadu state board','maharashtra board--1st maharashtra board','maharashtra board--2nd maharashtra board','maharashtra board--3rd maharashtra board','maharashtra board--4th maharashtra board','maharashtra board--5th maharashtra board','maharashtra board--6th maharashtra board','maharashtra board--7th maharashtra board','maharashtra board--8th maharashtra board','maharashtra board--9th maharashtra board','maharashtra board--10th maharashtra board','maharashtra board--11th maharashtra board','maharashtra board--12th maharashtra board','rajasthan board--1st rbse','rajasthan board--2nd rbse','rajasthan board--3rd rbse','rajasthan board--4th rbse','rajasthan board--5th rbse','rajasthan board--6th rbse','rajasthan board--7th rbse','rajasthan board--8th rbse','rajasthan board--9th rbse','rajasthan board--10th rbse','rajasthan board--11th rbse','rajasthan board--12th rbse','engineering--jee main','engineering--jee advanced','engineering--mht-cet','engineering--bitsat','engineering--ap eamcet','engineering--viteee','engineering--viteee (with biology)','engineering--kcet (ug)','engineering--srmjeee (ug)','engineering--bitsat (with biology)','engineering--amu-at (b.tech.)','engineering--ts eamcet','medical--ap eamcet','medical--neet','medical--ts eamcet','banking--bihar cooperative bank assistant prelims','banking--rbi assistant prelims','banking--ibps po mains','banking--ibps rrb officer scale-i prelims','banking--ibps clerk prelims','banking--ibps clerk mains','banking--ibps rrb office assistant mains','banking--ibps rrb office assistant prelims','banking--ibps po prelims','banking--sbi po mains','banking--ibps rrb officer scale-i mains','banking--sbi clerk prelims','banking--sbi clerk mains','banking--sbi po prelims','insurance--lic aao prelims','insurance--niacl assistant prelims','insurance--lic assistant prelims','insurance--insurance','railways--rpf sub inspector cbt 1','railways--rpf constable cbt 1','railways--rrc group d cbt','railways--rrb ntpc cbt 1','railways--railways','ssc--ib security assistant or executive tier 1','ssc--ssc stenographer grade c and d','ssc--ssc cpo paper-i','ssc--delhi police constable cbt','ssc--ssc mts tier i','ssc--ssc chsl tier-i','ssc--ssc cgl tier-i','ssc--ssc','teaching--ctet paper 1','teaching--ctet paper 2','defence--drdo mts tier i','defence--cisf head constable','defence--defence','jammu and kashmir board--1st jammu and kashmir board','jammu and kashmir board--2nd jammu and kashmir board','jammu and kashmir board--3rd jammu and kashmir board','jammu and kashmir board--4th jammu and kashmir board','jammu and kashmir board--5th jammu and kashmir board','jammu and kashmir board--6th jammu and kashmir board','jammu and kashmir board--7th jammu and kashmir board','jammu and kashmir board--8th jammu and kashmir board','jammu and kashmir board--9th jammu and kashmir board','jammu and kashmir board--10th jammu and kashmir board','jammu and kashmir board--11th jammu and kashmir board','jammu and kashmir board--12th jammu and kashmir board','national recruitment agency--nra cet - graduate level','national recruitment agency--nra cet - higher secondary level','national recruitment agency--nra cet - matriculation level','telangana board--1st telangana state board','telangana board--2nd telangana state board','telangana board--3rd telangana state board','telangana board--4th telangana state board','telangana board--5th telangana state board','telangana board--6th telangana state board','telangana board--7th telangana state board','telangana board--8th telangana state board','telangana board--9th telangana state board','telangana board--10th telangana state board','telangana board--11th telangana state board','telangana board--12th telangana state board','karnataka board--1st karnataka state board','karnataka board--2nd karnataka state board','karnataka board--3rd karnataka state board','karnataka board--4th karnataka state board','karnataka board--5th karnataka state board','karnataka board--6th karnataka state board','karnataka board--7th karnataka state board','karnataka board--8th karnataka state board','karnataka board--9th karnataka state board','karnataka board--10th karnataka state board','karnataka board--11th karnataka state board','karnataka board--12th karnataka state board','kerala board--1st kerala board','kerala board--2nd kerala board','kerala board--3rd kerala board','kerala board--4th kerala board','kerala board--5th kerala board','kerala board--6th kerala board','kerala board--7th kerala board','kerala board--8th kerala board','kerala board--9th kerala board','kerala board--10th kerala board','kerala board--11th kerala board','kerala board--12th kerala board','uttar pradesh board--9th uttar pradesh board','uttar pradesh board--10th uttar pradesh board','uttar pradesh board--11th uttar pradesh board','uttar pradesh board--12th uttar pradesh board']
	# active_goal_exams = ['cbse--10th cbse','engineering--bitsat']
	lms = {}
	for goal_exam in active_goal_exams:
		cursor = lm_collection.find({'status':'active','learnpath_name':{"$regex":goal_exam}})

		for doc in cursor:
			if len(doc['learnpath_name'].split("--")) >= 2:
				lms[doc["code"]] = doc['learnpath_name'].split("--")[0] + "--" + doc['learnpath_name'].split("--")[1]

		client.close()
		# break

	return lms

def get_question_hygiene():
	query = {}
	query["type"] = "Question"
	query["status"] = "Published"
	query["content.question_meta_tags.source"] = {"$in":["OT","funtoot","mockbank"]}
	query["content.question_meta_tags.primary_concept"] = None

	projection = {}
	projection["id"] = 1.0
	projection["question_code"] = 1.0
	projection["purpose"] = 1.0
	projection["content.question_meta_tags.learning_maps"] = 1.0
	projection["content.book_meta_tags"] = 1.0

	# projection["content.question_meta_tags.bloom_level"] = 1.0
	all_active_lms = get_active_lms()
	# print(all_active_lms)
	print("get_question_hygiene")
	cursor = collection.find(query, projection = projection)#.limit(1000)
	questions = []
	try:
		# print(cursor)
		all_learning_maps = []
		all_primary_concepts = []
		# print(len(list(cursor)))
		for doc in cursor:
			# print("\t",doc['id'])
			try:
				question_meta_data = {}
				question_meta_data['id'] = doc['id']
				question_meta_data['question_code'] = doc['question_code']
				 
				question_meta_data['purpose'] = str(set(doc.get('purpose',[])))[1:-1]
				if question_meta_data['purpose'] == "et(":
					question_meta_data['purpose'] = ""
					
				question_meta_tags = doc.get("content",{}).get("question_meta_tags",[])
				if len(question_meta_tags) == 0:
					question_meta_data["learning_maps"] = "question_meta_tags key not available"
				else:
					question_meta_tags = question_meta_tags[0]
					learning_maps = question_meta_tags.get("learning_maps",[])
					
					lm_in_active_exams = False
					goal_exams = []
					for lm in learning_maps:
						if lm in all_active_lms:
							lm_in_active_exams = True
							goal_exams.append(all_active_lms[lm])


					if not lm_in_active_exams:
						continue

					question_meta_data['learning_maps'] = learning_maps
					question_meta_data['goal_exams'] = str(set(goal_exams))[1:-1]

				question_meta_data['is_book_question'] = doc.get("content",{}).get("book_meta_tags","No")
				if question_meta_data['is_book_question'] != "No":
					question_meta_data['is_book_question'] = "Yes"

				questions.append(question_meta_data)
			except Exception as e:
				print(doc['question_code'])
				print(e)

		df = pd.DataFrame(questions)
		
		df.to_csv("Results/question_hygiene_check_results.csv",index=False)
		print(len(df))
			
	finally:
		# print("Failed")
		client.close()


if os.path.exists('Results'):
	os.system("rm -r Results")

os.system("mkdir Results")

# time.sleep(8000)
get_question_hygiene()
